package br.com.userede;

public class Funcionario extends Pessoa{
    private String numIdentificacao;

    public Funcionario(){

    }

    public Funcionario(String numIdentificacao) {
        this.numIdentificacao = numIdentificacao;
    }

    public Funcionario(String nome, String cpf, String numIdentificacao) {
        super(nome, cpf);
        this.numIdentificacao = numIdentificacao;
    }

    public String getNumIdentificacao() {
        return numIdentificacao;
    }

    public void setNumIdentificacao(String numIdentificacao) {
        this.numIdentificacao = numIdentificacao;
    }

    @Override
    public String getNome() {
        return super.getNome();
    }

    @Override
    public void setNome(String nome) {
        super.setNome(nome);
    }

    @Override
    public String getCpf() {
        return super.getCpf();
    }

    @Override
    public void setCpf(String cpf) {
        super.setCpf(cpf);
    }

    public void cadastrarFuncionario(){
        System.out.println("Digite o nome do funcionário");
        String nome = IO.criaScanner().nextLine();
        setNome(nome);
        System.out.println("Insira o número de identificação do funcionário");
        String numId = IO.criaScanner().nextLine();
        setNumIdentificacao(numId);
    }
    public void exibirFuncionario(){
        System.out.println("Nome do funcionário: ");
        System.out.println(getNome());
        System.out.println("Número de identificação do funcionário: ");
        System.out.println(getNumIdentificacao());
    }

    @Override
    public String toString() {
        return "Funcionario{" +
                "numIdentificacao='" + numIdentificacao + '\'' +
                '}';
    }
}
