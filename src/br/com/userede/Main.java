package br.com.userede;

import java.util.Scanner;
//import br.com.userede.Imovel;
//import br.com.userede.Morador;
//import br.com.userede.Pessoa;

public class Main {

    public static void main(String[] args) {
        Scanner leitor = new Scanner(System.in);
        Pessoa gente = new Pessoa();
        Funcionario humano = new Funcionario();
        Morador morador = new Morador();
        Imovel casa = new Imovel();
        IO input = new IO();

        String endereco = "n";
        double valorAluguel = 0.0;
        int opcao;

        System.out.println("TESTANDO MUDANÇA NO CÓDIGO");
        System.out.println("Digite 1 para cadastrar imóvel/morador");
        System.out.println("Digite 2 para exibir informações");
        System.out.println("Digite 3 para excluir um imóvel");

        opcao = leitor.nextInt();
        if (opcao==1){
            casa.cadastrarImovel(endereco, valorAluguel);
        }
        else if (opcao==2){
            morador.exibirMorador();
        }
        else if (opcao==3){
            System.out.println("OPÇÃO AINDA NÃO DISPONÍVEL :(");
        }
    }
}
