package br.com.userede;

//import java.util.ArrayList;
//import java.util.List;

public class Imovel {
    private String endereco;
    private  Morador moradores;
    double  valorAluguel;

    public Imovel(){

    }

    public Imovel(String endereco, Morador moradores, double valorAluguel) {
        this.endereco = endereco;
        this.moradores = moradores;
        this.valorAluguel = valorAluguel;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public Morador getMoradores() {
        return moradores;
    }

    public void setMoradores(Morador moradores) {
        this.moradores = moradores;
    }

    public double getValorAluguel() {
        return valorAluguel;
    }

    public void setValorAluguel(double valorAluguel) {
        this.valorAluguel = valorAluguel;
    }

    public static Morador adicionaMorador() {
        System.out.println("Digite o nome do morador");
        String nome = IO.criaScanner().nextLine();
        System.out.println("Digite o CPF do morador");
        String cpf = IO.criaScanner().nextLine();
        System.out.println("Digite o número do contrato do morador");
        String numeroContrato = IO.criaScanner().nextLine();
        Morador morador = new Morador(nome, cpf, numeroContrato);
        return morador;
    }

    public void cadastrarImovel(String endereco, double valorAluguel){
        System.out.println("Digite o endereço do imóvel");
        endereco = IO.criaScanner().nextLine();
        this.endereco = endereco;
        System.out.println("Digite o valor do aluguel");
        valorAluguel = IO.criaScanner().nextDouble();
        this.valorAluguel = valorAluguel;
        Morador moradores = Imovel.adicionaMorador();
    }

    public void exibirImovel(){
        System.out.println("Dados do imóvel: ");
        System.out.println("***********************");
        System.out.println("Endereço: ");
        System.out.println(getEndereco());
        System.out.println("Valor do aluguel");
        System.out.println(getValorAluguel());
    }
}
