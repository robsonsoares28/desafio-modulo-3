package br.com.userede;

public class Morador extends Pessoa{
    Imovel c1 = new Imovel();

    private  String numeroContrato;

    public Morador() {

    }

    public Morador(String nome, String cpf, String numeroContrato) {
        super(nome, cpf);
        this.numeroContrato = numeroContrato;
    }

    public String getNumeroContrato() {
        return numeroContrato;
    }

    public void setNumeroContrato(String numeroContrato) {
        this.numeroContrato = numeroContrato;
    }

    @Override
    public String getNome() {
        return super.getNome();
    }

    @Override
    public void setNome(String nome) {
        super.setNome(nome);
    }

    @Override
    public String getCpf() {
        return super.getCpf();
    }

    @Override
    public void setCpf(String cpf) {
        super.setCpf(cpf);
    }

    public void exibirMorador(){
        System.out.println("Dados do morador");
        System.out.println("********************");
        System.out.println("Nome do morador: ");
        System.out.println(getNome());
        System.out.println("CPF do morador: ");
        System.out.println(getCpf());
        System.out.println("Número de contrato");
        System.out.println(getNumeroContrato());
    }

    @Override
    public String toString() {
        return "Morador{" +
                "numeroContrato='" + numeroContrato + '\'' +
                '}';
    }
}